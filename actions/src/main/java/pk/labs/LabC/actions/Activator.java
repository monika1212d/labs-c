package pk.labs.LabC.actions;

import java.util.Collection;
import java.util.HashSet;
import java.util.Hashtable;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;


import pk.labs.LabC.contracts.Animal;
import pk.labs.LabC.contracts.AnimalAction;

public class Activator implements BundleActivator{
      public static BundleContext context;
        @Override
  public void start(BundleContext bc)
  {
      context=bc; 
      
    AnimalAction a1 = new AnimalAction()
    {
      public String toString() {
        return "Nakarm";
      }

      public boolean execute(Animal animal)
      {
        animal.setStatus("Nakarmione");
        return true;
      }
    };
        AnimalAction a2 = new AnimalAction()
    {
      public String toString() {
        return "Wyczesz";
      }

      public boolean execute(Animal animal)
      {
        animal.setStatus("Wyczesane");
        return true;
      }
    };
            AnimalAction a3 = new AnimalAction()
    {
      public String toString() {
        return "Wypu��";
      }

      public boolean execute(Animal animal)
      {
        animal.setStatus("Na wybiegu");
        return true;
      }
    };
            Hashtable<String, String> propa1= new Hashtable<String, String>();
            propa1.put("species", "Zwierze1");
            Hashtable<String, String[]> propa2= new Hashtable<String, String[]>();
            propa2.put("species", new String[]{"Zwierze2"});
            Hashtable<String, String> propa3= new Hashtable<String, String>();
            propa3.put("species", "Zwierze3"); 
       bc.registerService(pk.labs.LabC.contracts.AnimalAction.class.getName(), a1, null);
       bc.registerService(pk.labs.LabC.contracts.AnimalAction.class.getName(), a2, propa2);
       bc.registerService(pk.labs.LabC.contracts.AnimalAction.class.getName(), a3, null); 
  }

    @Override
    public void stop(BundleContext bc)
    {
        context=null;
    }
}
