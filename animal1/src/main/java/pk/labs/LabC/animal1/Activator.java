package pk.labs.LabC.animal1;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import pk.labs.LabC.contracts.Animal;

public class Activator implements BundleActivator{
     private static BundleContext context;
       @Override
    public void start(BundleContext bc) {
        		context = bc;
	    context.registerService(Animal.class.getName(), new Zwierze1(), null);
            pk.labs.LabC.logger.Logger.get().log(this, "Przybywa Zwierze 1");
    }

    @Override
    public void stop(BundleContext bc) {
        context = null;
        pk.labs.LabC.logger.Logger.get().log(this, "Odchodzi Zwierze 1");
    }
}
