package pk.labs.LabC.animal2;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import pk.labs.LabC.contracts.Animal;

public class Zwierze2 implements  Animal {
  
    private String status;
    private static PropertyChangeSupport listeners;
    public Zwierze2(){
        listeners= new PropertyChangeSupport(this);
    }
    @Override
    public String getSpecies() {
        return "Zwierze 2";
    }

    @Override
    public String getName() {
        return "Nazwa zwierzecia 2";
    }

    @Override
    public String getStatus() {
        return status;
    }

    @Override
    public void setStatus(String status) {
        String oldValue=this.status;
        this.status=status;
        listeners.firePropertyChange( new PropertyChangeEvent(this, "status", oldValue, status));
    }

    @Override
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        listeners.addPropertyChangeListener(listener);
    }

    @Override
    public void removePropertyChangeListener(PropertyChangeListener listener) {
        listeners.removePropertyChangeListener(listener);
    }
}
